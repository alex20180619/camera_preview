package alex.com.camerapreview;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
//import android.graphics.Camera;
//import android.hardware.Camera;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by liufangyuan on 2018/5/9.
 */

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder mHolder;
    private static final String TAG = "CameraPreview";
    private Camera mCamera;
    Camera.PictureCallback raw;
    Camera.PictureCallback jpeg;
    Camera.ShutterCallback shutter;
    private final long RESERVE_EXTERNAL_STORAGE = 100000000; //100M
    private static final String BASE_DIR = Environment.getExternalStorageDirectory().toString();
    private final String CAM_PIC_DIR = BASE_DIR + "/DCIM";
    private ContentResolver mContentResolver = getContext().getContentResolver();

    private long getSDCardAvailableSize() {
        File path = Environment.getExternalStorageDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSizeLong();
        long availableBlocks = stat.getAvailableBlocksLong();
        Log.d(TAG, "getSDCardAvailableSize: blocksize = " + blockSize + ", availableBlocks = " + availableBlocks);
        Log.d(TAG, "getSDCardAvailableSize: availableSize = " + availableBlocks * blockSize);

        return blockSize * availableBlocks;
    }

    public String generatePictureName(long dataTaken) {
        Date date = new Date(dataTaken);
        SimpleDateFormat format = new SimpleDateFormat("'IMG'_yyyyMMdd_HHmmss");
        String result = format.format(date);
        Log.d(TAG, "generatePictureName: " + result);
        return result;
    }

    private void storeImage(final byte[] data) {
        long dataTaken = System.currentTimeMillis();
        String title = generatePictureName(dataTaken);
        Camera.Size s = mCamera.getParameters().getPictureSize();
        Log.d(TAG, "storeImage: pic size:" + s.width + "x" + s.height);
        addImage(title, dataTaken, data, s.width, s.height);
    }

    public void addImage(String title, long date, byte[] jpeg, int width, int height) {
        String picDirectory = CAM_PIC_DIR;

        File dir = new File(picDirectory);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        String path = picDirectory + '/' + title + ".jpeg";
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(path);
            out.write(jpeg);
        } catch (Exception e) {
            Log.e(TAG, "addImage: Failed to write image");
            return;
        } finally {
            try {
                out.close();
            } catch (Exception e) {
                Log.e(TAG, "addImage: close " + path + " failed");
            }
        }

        // Insert into MediaStore
        ContentValues values = new ContentValues(9);
        values.put(MediaStore.Images.ImageColumns.TITLE, title);
        values.put(MediaStore.Images.ImageColumns.DISPLAY_NAME, title + ".jpg");
        values.put(MediaStore.Images.ImageColumns.DATE_TAKEN, date);
        values.put(MediaStore.Images.ImageColumns.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.Images.ImageColumns.DATA, path);
        values.put(MediaStore.Images.ImageColumns.SIZE, jpeg.length);
        values.put(MediaStore.Images.ImageColumns.WIDTH, width);
        values.put(MediaStore.Images.ImageColumns.HEIGHT, height);
        Uri uri = mContentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        if (uri == null) {
            Log.e(TAG, "addImage: failed to write media store");
            return;
        }
    }

    public CameraPreview(Context context) {
        super(context);
        mHolder = getHolder();
        mHolder.addCallback(this);
        jpeg = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                Log.d(TAG, "onPictureTaken");
                mCamera.startPreview();
                if (getSDCardAvailableSize() < RESERVE_EXTERNAL_STORAGE) {
                    Log.d(TAG, "onPictureTaken: full");
                    return;
                }

                Toast.makeText(getContext(), "拍照成功，请在相册中查看", Toast.LENGTH_LONG).show();

                storeImage(data);
            }
        };
    }

    private static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(0);
        } catch (Exception e) {
            Log.d(TAG, "getCameraInstance: camera is not available");
        }
        return c;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mCamera = getCameraInstance();
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d(TAG, "surfaceCreated: error setting camera preview:" + e.getMessage());
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mHolder.removeCallback(this);
        mCamera.setPreviewCallback(null);
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        int rotation = getDisplayOrientation();
        mCamera.setDisplayOrientation(rotation);

    }

    public int getDisplayOrientation() {
        Display display = ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();
        int degress = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degress = 0;
                break;
            case Surface.ROTATION_90:
                degress = 90;
                break;
            case Surface.ROTATION_180:
                degress = 180;
                break;
            case Surface.ROTATION_270:
                degress = 270;
                break;
        }

        android.hardware.Camera.CameraInfo camInfo = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_FRONT, camInfo);
        int result = (360 + 180 + camInfo.orientation - degress) % 360;
        return result;
    }

    public void destroy() {
        mHolder.removeCallback(this);
        if (mCamera != null) {
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    public void takePic() {
        Log.d(TAG, "takePic");
        if (mCamera != null) {
            mCamera.takePicture(null, null, null, jpeg);
        }
    }
}
