package alex.com.camerapreview;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

public class MainActivity extends Activity implements View.OnClickListener {
    CameraPreview mPreview;
    FrameLayout preview;
    String TAG = "CameraPreview";
    private Button photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
        }

        if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }

        if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }

        preview = findViewById(R.id.cameraPreview);
        photo = findViewById(R.id.takePic);
        photo.setOnClickListener(this);
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        mPreview.destroy();
        super.onStop();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
        mPreview.destroy();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
        mPreview.destroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPreview = new CameraPreview(this);
        preview.addView(mPreview);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.takePic:
                mPreview.takePic();
                break;
            default:
                break;
        }
    }
}
